<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostLanguage extends Model
{
    protected $table = 'post_lang';
    //
    public function description()
    {
        return $this->hasMany('App\PostDescription', 'lang_id', 'lang_id');
    }
}
