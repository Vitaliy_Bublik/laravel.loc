<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::resource('/', 'IndexController', [
                                         'only'  => ['index'],
                                         'names' => ['index' => 'home']
                                        ]);

$router->get('/view/{id}',[
    'uses' => 'IndexController@show',
    'as'   => 'show'
]);



//Route::get('/', function () {
//    return view('welcome');
//});

Route::auth();

Route::get('/home', 'HomeController@index');



///

Route::resource('/edit', 'IndexController@create');

///

dump($_POST);