<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;
use App\PostLanguage;


class IndexController extends Controller
{

    public function index()
    {
        //$query = Post::with('description')->get();
        //$query = Post::all();

        $articles = Post::with('description')->paginate(2);
       // $languages = PostLanguage::with('description')->get();

        //dump($languages);


        return view(env('THEME').'.site', ['articles' => $articles]);

        //
        //
        //return view(env('THEME').'.site')->with('query', $query);
    }

    public function create()
    {
        //create post (form)

        $articles = Post::with('description')->get();
        $articles = $articles->keyBy('post_id');

        $languages = PostLanguage::with('description')->get();

        return view(env('THEME').'.edit', ['articles' => $articles, 'languages' => $languages]);
    }

    public function store(Request $request)
    {
        //save new post [post]
    }

    public function show($id)
    {
        //view post [get]
        $articles = Post::with('description')->get();
        $languages = PostLanguage::with('description')->get();

        return view(env('THEME').'.post', ['articles' => $articles, 'languages' => $languages]);
    }

    public function edit($id)
    {
        //wiew form post [get]
    }

    public function update(Request $request, $id)
    {
        //update post of db
    }

    public function destroy($id)
    {
        //delete
    }
}
