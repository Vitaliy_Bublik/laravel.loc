<?php

return [
    'title' => 'Users',
    'single' => 'user',
    'model' => 'App\User',
    'column' => [
        'id',
        'email',
    ],
    'edit_fields' =>[
        'email' => [
            'type' => 'text',
        ]
    ]
];