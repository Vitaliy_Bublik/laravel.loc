@extends('block.layout.layout')

        <!-- NAVIGATION MENU -->
@section('navmenu')
    <div class="navbar-nav navbar-inverse navbar-fixed-top">
        <div class="container">

            <a class="navbar-brand" href=" {{ route('home') }} "><img src="http://www.prepbootstrap.com/Content/images/shared/single-page-admin/logo30.png" alt=""> AMIGOS</a>

            <ul class="nav navbar-nav" style="float: right">
                <li><a href=" {{ route('home') }}"><i class="icon-lock icon-white"></i> Login</a></li>
            </ul>

        </div>
    </div>
@endsection


@section('container')
    <div class="container">
        @foreach($articles as $posts)

            <div class="col-md-12">
                <div class="well row">

                    <div class="col-md-12" style="text-align: center">
                        <h3>
                            <a href="{!! route('show', ['id'=>$posts->seo_url]) !!}">

                                {!!
                                    $posts->description->first()->title or '<p style="color: red">'.'- Empty -'.'</p>'
                                !!}

                            </a>
                        </h3>
                    </div>

                    <div class="col-md-12">
                        <h4 style="color:green;">{{ $posts->seo_url }}</h4>
                    </div>

                    <div class="col-md-4">
                        <img src="{{
                                     $posts->image != "" ?
                                     'public/block/upload/global/'.$posts->image :
                                     URL::asset('public/block/upload/empty.png')
                                  }}"
                                     alt="some_text" style="width: 250px"
                        >
                    </div>

                    <div class="col-md-8">
                        <h4>
                            {!! $posts->description->first()->post or '<p style="color: red">'.'- Empty -'.'</p>' !!}
                        </h4>
                    </div>
                </div>
            </div>



        @endforeach
    </div>
@endsection


@section('footer')
    <div class="text-center">
        {{ $articles->links() }}

</div>
@endsection