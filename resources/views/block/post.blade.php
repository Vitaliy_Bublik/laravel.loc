@extends('block.layout.layout')

        <!-- NAVIGATION MENU -->
@section('navmenu')
    <div class="navbar-nav navbar-inverse navbar-fixed-top">
        <div class="container">

            <a class="navbar-brand" href=" {{ route('home') }} ">
                <img src="http://www.prepbootstrap.com/Content/images/shared/single-page-admin/logo30.png" alt=""> AMIGOS
            </a>

            <ul class="nav navbar-nav" style="float: right">
                <li><a href="login.html"><i class="icon-lock icon-white"></i> Login</a></li>
            </ul>

        </div>
    </div>
@endsection

@section('container')
    <div class="container">

        <!--Tabs-form-->
        <div class="col-md-2">
            <ul class="nav nav-pills nav-stacked">

                @foreach ($languages as $key => $lang)


                    <li class="{!! $lang->id == '1' ? 'active' : '' !!}}">
                        <a data-toggle="tab" href="#{{ $lang->lang_id }}">{!! $lang->lang_id ? $lang->lang_id : '???'  !!}</a>
                    </li>

                @endforeach

            </ul>
        </div>
        <!--End tabs-form-->

        <div class="tab-content post-create col-md-10 well">
            <!--DESCRIPTION-->
            <?php $i=0; ?>
            @foreach ($languages as $key => $lang)
                <?php $i++; ?>


                <div id="{{ $lang->lang_id }}" class="tab-pane fade{!!  $i<=1 ? ' active in' : '' !!}">
                    <div class="well row">


                        <!--Views post-->
                        @if($lang->lang_id)

                            <div class="col-md-10">

                                <div class="col-md-12" style="text-align: center">
                                    <h3>{!! $lang->description->first()->post or '<p style="color: red">'.'- Empty -'.'</p>' !!}</h3>
                                </div>

                                <div class="col-md-12">
                                    <h4 style="color:green;">{{ $lang->seo_url }}</h4>
                                </div>

                                <div class="col-md-12" style="text-align: center">
                                    <img src="{{
                                     $lang->image != "" ?
                                     'public/block/upload/global/'.$lang->image :
                                     URL::asset('public/block/upload/empty.png')
                                          }}"
                                         alt="some_text" style="width: 300px"
                                    >
                                </div>

                                <div class="col-md-12" style="text-align: center">
                                    <h4 style="text-align: left">
                                        {!! $lang->description->first()->post or '<p style="color: red">'.'- Empty -'.'</p>' !!}
                                    </h4>
                                </div>

                            </div>

                        @else

                            <div class="col-md-12 well row">
                                <h1 class="text-center">Статьи не существует</h1>
                            </div>

                            @endif
                                    <!--End views post-->

                    </div>
                </div>

            @endforeach
        </div>

    </div>
@endsection


@section('footer')
@endsection


