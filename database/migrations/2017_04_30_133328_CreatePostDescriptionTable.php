<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostDescriptionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_description', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id');
            $table->string('lang_id');
            $table->string('post');
            $table->string('title');
            $table->string('description');
            $table->string('mt');
            $table->string('md');
            $table->string('keywords');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post_description');
    }
}
